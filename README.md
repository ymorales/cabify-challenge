# README #

### Solution features ###
* Configurable rules for discounts
* Stock class to extend products
* Original price and final price included in Invoice
* Currency conversion with configurable currencies
* BDD with Rspec
* Code coverage with simplecov

### Requirements ###
* ruby >= 2.0
* rspec (to test)

### How to install ###
In your shell please execute

```
#!bash
$git clone https://ymorales@bitbucket.org/ymorales/cabify-challenge.git
$cd cabify-challenge
$bundle install --path .bundle
$ruby challenge.rb
```

### Output ###
```
#!bash
----- INVOICE -----
Items: Cabify Voucher,Cabify T-Shirt,Cabify Voucher,Cabify Voucher,Cafify Coffee Mug,Cabify T-Shirt,Cabify T-Shirt
Total pre-discount: 82.5 €
Total: 74.5 €
------------------
```

### How to install ###
In your shell please execute
```
#!bash
$rspec spec
...............

Finished in 0.01577 seconds (files took 0.75142 seconds to load)
15 examples, 0 failures

Coverage report generated for RSpec to /Users/yohanmorales/projects/cabify_challenge/coverage. 84 / 85 LOC (98.82%) covered.
```

### Code coverage ###
![Captura de pantalla 2016-10-17 a las 9.36.18 p.m..png](https://bitbucket.org/repo/9rnMX7/images/3782897105-Captura%20de%20pantalla%202016-10-17%20a%20las%209.36.18%20p.m..png)

### Contact ###
Yohan Alejandro Morales
yohanmorales@hotmail.com
Buenos Aires, Argentina