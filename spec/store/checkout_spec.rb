require 'spec_helper'

describe Checkout do
  describe '.new' do
    context 'checkout initialization' do
      it 'set attributes' do
        checkout = Checkout.new
        expect(checkout.store_factory).to be_an_instance_of(PhysicalItemFactory)
        expect(checkout.rules).to be_an_instance_of(Array)
        expect(checkout.stock).to be_an_instance_of(Stock)
      end
    end
  end

  describe 'scan items' do
    context 'item addition' do
      it 'set new item' do
        checkout = Checkout.new
        checkout.scan('VOUCHER')
        expect(checkout.get_items.last).to be_an_instance_of(Item)
      end
    end
  end

  describe 'add rule' do
    context 'rule addition' do
      it 'set new rule' do
        checkout = Checkout.new
        marketing_engine = MarketingEngine.new
        two_for_one_disc = marketing_engine.available_discounts[:two_for_one]
        one_to_one_disc  = marketing_engine.available_discounts[:one_to_one]
        three_items_disc = marketing_engine.available_discounts[:n_same_items]
        two_for_one    = DiscountRule.new('VOUCHER', 2, 0.5,  two_for_one_disc)
        one_to_one     = DiscountRule.new('MUG',     1, 1.0,  one_to_one_disc)
        three_discount = DiscountRule.new('TSHIRT',  3, 0.95, three_items_disc)
        checkout.add_rule(two_for_one)
        checkout.add_rule(one_to_one)
        checkout.add_rule(three_discount)

        expect(checkout.rules[0]).to be_an_instance_of(DiscountRule)
        expect(checkout.rules[1]).to be_an_instance_of(DiscountRule)
        expect(checkout.rules[2]).to be_an_instance_of(DiscountRule)
      end
    end
  end

  describe 'apply rules' do
    context 'basic rules' do
      it 'apply cabify rules' do
        checkout = Checkout.new
        marketing_engine = MarketingEngine.new
        two_for_one_disc = marketing_engine.available_discounts[:two_for_one]
        one_to_one_disc  = marketing_engine.available_discounts[:one_to_one]
        three_items_disc = marketing_engine.available_discounts[:n_same_items]
        two_for_one    = DiscountRule.new('VOUCHER', 2, 0.5,  two_for_one_disc)
        one_to_one     = DiscountRule.new('MUG',     1, 1.0,  one_to_one_disc)
        three_discount = DiscountRule.new('TSHIRT',  3, 0.95, three_items_disc)
        checkout.add_rule(two_for_one)
        checkout.add_rule(one_to_one)
        checkout.add_rule(three_discount)
        checkout.scan('VOUCHER')
        checkout.scan('TSHIRT')
        checkout.scan('VOUCHER')
        checkout.scan('VOUCHER')
        checkout.scan('MUG')
        checkout.scan('TSHIRT')
        checkout.scan('TSHIRT')

        expect(checkout.apply_rules).to eql(81.976075)
        expect(checkout.calculate_plain_price.round(2)).to eql(90.78)
      end
    end
  end
end
