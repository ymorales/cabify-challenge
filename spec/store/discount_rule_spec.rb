require 'spec_helper'

describe DiscountRule do

  describe '.new' do
    context 'discount initialization' do
      it 'set discount attributes' do
        discount_rule = DiscountRule.new('GROCERIES', 2, 0.5, 'count * price')
        expect(discount_rule.filter).to eql('GROCERIES')
        expect(discount_rule.quantity).to eql(2)
        expect(discount_rule.formula).to eql('count * price')
      end
    end
  end

  describe 'calculate' do
    context 'calculate a discount' do
      it 'set discount for voucher items' do
        discount_rule = DiscountRule.new('VOUCHER', 2, 0.5, '((count / @quantity) * price) + ((count % @quantity) * price)')
        item_factory  = ItemFactory.new
        store_factory = item_factory.get_factory('PhysicalItem')
        stock = Stock.new
        items = Array.new
        item1 = store_factory.build_item(stock.stock_items.find {|x| x[:code] == 'VOUCHER'})
        item2 = store_factory.build_item(stock.stock_items.find {|x| x[:code] == 'VOUCHER'})
        items << item1
        items << item2

        price_with_discount = discount_rule.calculate(items)
        expect(price_with_discount).to eql(5.50175)
      end

      it 'set discount for non-stock items' do
        discount_rule = DiscountRule.new('ECARDS', 2, 0.5, '((count / @quantity) * price) + ((count % @quantity) * price)')
        item_factory  = ItemFactory.new
        store_factory = item_factory.get_factory('PhysicalItem')
        stock = Stock.new
        items = Array.new
        item1 = store_factory.build_item(stock.stock_items.find {|x| x[:code] == 'VOUCHER'})
        item2 = store_factory.build_item(stock.stock_items.find {|x| x[:code] == 'VOUCHER'})
        items << item1
        items << item2

        price_with_discount = discount_rule.calculate(items)
        expect(price_with_discount).to eql(0)
      end
    end
  end

end
