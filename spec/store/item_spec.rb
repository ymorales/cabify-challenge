require 'spec_helper'

describe Item do

  describe '.new' do
    #test for empty initialize
    context 'giving empty attributes' do
      it 'returns None, price 0.0' do
        item = Item.new
        expect(item.code).to eql('None')
        expect(item.price).to eql(0.0)
        expect(item.name).to eql('empty product')
      end
    end
  end

end
