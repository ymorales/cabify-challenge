require 'spec_helper'

describe ItemFactory do

  describe 'physical item' do
    #test for physical item factory
    context 'get factory for physical item' do
      it 'returns PhysicalItemFactory object' do
        item_factory  = ItemFactory.new
        store_factory = item_factory.get_factory('PhysicalItem')
        expect(store_factory).to be_an_instance_of(PhysicalItemFactory)
      end

      it 'creates an item' do
        item_factory  = ItemFactory.new
        store_factory = item_factory.get_factory('PhysicalItem')
        item = store_factory.build_item({})
        expect(item).to be_an_instance_of(Item)
      end
    end
  end

end
