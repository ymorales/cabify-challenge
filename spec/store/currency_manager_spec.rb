require 'spec_helper'

describe CurrencyManager do
  describe '.new' do
    context 'currency initialization' do
      it 'returns hash o currencies' do
        currencies = {
          dollar: { equivalence: 1, symbol: 'USD' },
          euro:   { equivalence: 0.908801745, symbol: '€' },
          gbp:    { equivalence: 0.82176021, symbol: '$GBP' }
        }
        currency_manager = CurrencyManager.new
        expect(currency_manager.available_currencies).to include(currencies)
      end
    end

    context 'currency conversion' do
      it 'returns an euro conversion' do
        currency_manager = CurrencyManager.new
        expect(currency_manager.convert_to_currency(5.0, :euro, 2)).to eql(4.54)
      end

      it 'returns an COP(colombian peso) conversion' do
        currency_manager = CurrencyManager.new
        expect(currency_manager.convert_to_currency(5.0, :cop, 2))
              .to eql(14534.88)
      end

      it 'returns an dollar conversion' do
        currency_manager = CurrencyManager.new
        expect(currency_manager.convert_to_currency(5.0, :bov)).to eql(5.0)
      end
    end

    context 'symbol conversion' do
      currency_manager = CurrencyManager.new
      it 'returns currency strings' do
        expect(currency_manager.get_currency_symbol(:bov)).to eql('USD')
        expect(currency_manager.get_currency_symbol(:dollar)).to eql('USD')
        expect(currency_manager.get_currency_symbol(:euro)).to eql('€')
        expect(currency_manager.get_currency_symbol(:cop)).to eql('$COP')
      end
    end

  end

end
