require 'simplecov'
SimpleCov.start do
  add_filter '.bundle'
  add_filter '.spec'
end
require_relative '../store/item'
require_relative '../store/item_factory'
require_relative '../store/physical_item_factory'
require_relative '../store/currency_manager'
require_relative '../store/discount_rule'
require_relative '../store/stock'
require_relative '../store/checkout'
require_relative '../store/marketing_engine'
require_relative '../store/discount_rule'
