class CurrencyManager
  attr_accessor :available_currencies

  def initialize
    @available_currencies = {
      dollar: { equivalence: 1, symbol: 'USD' },
      euro:   { equivalence: 0.908801745, symbol: '€' },
      cop:    { equivalence: 2906.97674, symbol: '$COP' },
      ars:    { equivalence: 15.0700002, symbol: '$ARS' },
      gbp:    { equivalence: 0.82176021, symbol: '$GBP' }
    }
  end

  def convert_to_currency(value, currency = :dollar, decimal_places = 2)
    if !@available_currencies[currency].nil?
      (value * @available_currencies[currency][:equivalence]).round(decimal_places)
    else
      (value * @available_currencies[:dollar][:equivalence]).round(decimal_places)
    end
  end

  def get_currency_symbol(currency)
    if !@available_currencies[currency].nil?
      @available_currencies[currency][:symbol]
    else
      @available_currencies[:dollar][:symbol]
    end
  end
end
