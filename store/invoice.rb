require_relative 'currency_manager'
require_relative 'printable_interface'

class Invoice < PrintableInterface
  attr_accessor :checkout, :currency_manager

  def initialize(checkout)
    @checkout = checkout
    @currency_manager = CurrencyManager.new
  end

  def to_printer(currency = :dollar)
    plain_price = self.checkout.calculate_plain_price
    final_price = self.checkout.apply_rules

    currency_symbol   = currency_manager.get_currency_symbol(currency)
    raw_in_currency   = currency_manager.convert_to_currency(plain_price, currency)
    total_in_currency = currency_manager.convert_to_currency(final_price, currency)

    puts '----- INVOICE -----'
    puts "Items: #{self.checkout.get_items.map(&:name).join(',')}"
    # comment to quit the plain price
    puts "Total pre-discount: #{raw_in_currency} #{currency_symbol}"
    puts "Total: #{total_in_currency} #{currency_symbol}"
    puts '------------------'
  end
end
