class Item
  attr_accessor :code, :name, :price

  def initialize(attributes = {})
    @code  = attributes.fetch(:code,  'None')
    @name  = attributes.fetch(:name,  'empty product')
    @price = attributes.fetch(:price, 0.0)
  end
end
