class PrintableInterface
  def to_printer
    raise NotImplementedError
  end
end
