class MarketingEngine
  attr_accessor :available_discounts

  def initialize
    @available_discounts = {
      two_for_one:  '((count / @quantity) * price) + ((count % @quantity) * price)',
      one_to_one:   'count * price',
      n_same_items: 'count < @quantity ? price : count * price * @discount'
    }
  end
end
