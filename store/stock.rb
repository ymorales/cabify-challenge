class Stock
  attr_accessor :stock_items

  def initialize
    # prices in dollars as standard
    @stock_items = [
      { code: 'VOUCHER', name: 'Cabify Voucher',    price: 5.50175 },
      { code: 'TSHIRT',  name: 'Cabify T-Shirt',    price: 22.00700 },
      { code: 'MUG',     name: 'Cafify Coffee Mug', price: 8.252625 }
    ]
  end
end
