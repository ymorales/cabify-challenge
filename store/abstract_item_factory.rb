class AbstractItemFactory
  def build_item(attributes = {})
    raise NotImplementedError
  end
end
