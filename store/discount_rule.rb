class DiscountRule
  attr_accessor :formula, :quantity, :discount
  attr_accessor :filter

  def initialize(filter, quantity, discount, formula)
    @filter   = filter
    @quantity = quantity
    @discount = discount
    @formula  = formula
  end

  def calculate(items)
    filtered_items = items.select {|i| i.code == @filter}
    if !filtered_items.empty?
      count = filtered_items.count
      price = filtered_items.first.price
      return eval(@formula)
    else
      return 0
    end
  end
end
