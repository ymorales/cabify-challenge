require_relative 'abstract_item_factory'
require_relative 'item'

class PhysicalItemFactory < AbstractItemFactory
  def build_item(attributes = {})
    Item.new(attributes)
  end
end
