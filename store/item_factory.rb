require_relative 'physical_item_factory'

class ItemFactory
  def get_factory(factory_type)
    if (%w{PhysicalItem}).include? factory_type
      self.class.const_get("#{factory_type}Factory").new
    end
  end
end
