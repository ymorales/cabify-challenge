require_relative 'item_factory'
require_relative 'stock'

class Checkout
  attr_accessor :items, :stock, :rules, :total, :raw_total, :store_factory
  attr_accessor :rules

  def initialize
    @total = 0.0
    @raw_total = 0.0
    @items = Array.new
    @stock = Stock.new
    item_factory  = ItemFactory.new
    @store_factory = item_factory.get_factory('PhysicalItem')
    @rules = Array.new
  end

  def scan(item_code)
    if !@stock.stock_items.find { |x| x[:code] == item_code }.nil?
      item = @store_factory.build_item(@stock.stock_items.find { |x| x[:code] == item_code })
      @items << item if !item.nil?
    end
  end

  def add_rule(rule)
    @rules << rule
  end

  def apply_rules
    @total = 0.0
    @rules.each do |rule|
      @total += rule.calculate(items)
    end
    @total
  end

  def calculate_plain_price
    @raw_total = 0.0
    @items.each do |item|
      @raw_total += item.price
    end
    @raw_total
  end

  def get_items
    @items
  end
end
