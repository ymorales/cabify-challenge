require_relative 'store/checkout'
require_relative 'store/invoice'
require_relative 'store/discount_rule'
require_relative 'store/marketing_engine'

checkout = Checkout.new
marketing_engine = MarketingEngine.new

checkout.scan('VOUCHER')
checkout.scan('TSHIRT')
checkout.scan('VOUCHER')
checkout.scan('VOUCHER')
checkout.scan('MUG')
checkout.scan('TSHIRT')
checkout.scan('TSHIRT')

two_for_one_discount = marketing_engine.available_discounts[:two_for_one]
one_to_one_discount  = marketing_engine.available_discounts[:one_to_one]
three_items_discount = marketing_engine.available_discounts[:n_same_items]

two_for_one    = DiscountRule.new('VOUCHER', 2, 0.5,  two_for_one_discount)
one_to_one     = DiscountRule.new('MUG',     1, 1.0,  one_to_one_discount)
three_discount = DiscountRule.new('TSHIRT',  3, 0.95, three_items_discount)

checkout.add_rule(two_for_one)
checkout.add_rule(one_to_one)
checkout.add_rule(three_discount)

invoice = Invoice.new(checkout)

# un-comment to test in dollar currency
# invoice.to_printer(:dollar)
invoice.to_printer(:euro)
